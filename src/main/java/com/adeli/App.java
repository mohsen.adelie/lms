package com.adeli;

import com.adeli.dao.*;
import com.adeli.model.Book;
import com.adeli.model.Borrow;
import com.adeli.model.Login;
import com.adeli.model.User;
import com.adeli.view.Console;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


/**
 * Library Management System
 *
 * @author Mohsen Adeli
 * @version 1.0.0
 * @date 2/12/20
 */
public class App {
    private static User currentUser;
    private static Book selectedBook;
    private static UserDao userDao = new UserDaoImpl();
    private static BookDao bookDao = new BookDaoImpl();
    private static BorrowDao borrowDao = new BorrowDaoImpl();

    public static void main(String[] args) {
        mainMenu();
    }

    private static void mainMenu() {
        Console.clearConsole();
        System.out.println("------L.M.S------");
        System.out.println("1. Login");
        System.out.println("2. Register");
        System.out.println("3. Exit");
        int choice = (int) Console.readNumber("Enter a number: ", 1, 3);
        switch (choice) {
            case 1:
                loginMenu();
                break;
            case 2:
                registerMenu();
                break;
            case 3:
                System.out.println("Good Bye!");
                System.exit(0);
        }
    }

    private static void loginMenu() {
        Login login = new Login();
        int loginTry = 3;
        Console.clearConsole();
        System.out.println("------Login------");
        UserDao userDao = new UserDaoImpl();
        while (loginTry > 0) {
            login.setUsername(Console.readString("Enter your username: "));
            try {
                login.setPassword(Console.readPassword());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                currentUser = userDao.getUserByUserNameAndPassword(login);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (currentUser.getId() != null) {
                System.out.println();
                System.out.println("Welcome " + currentUser.getFirstName() + " " + currentUser.getLastName());

                afterLoginMenu();
                break;
            } else
                System.out.println("username/password is wrong!");
            loginTry--;
        }
    }

    public static void afterLoginMenu() {
        System.out.println("------------------");
        System.out.println("1- Change User Profile");
        System.out.println("2- Borrow a book");
        System.out.println("3- Return a book");
        System.out.println("4- Exit");
        int choice = (int) Console.readNumber("Enter a number: ", 1, 4);
        switch (choice) {
            case 1:
                // TODO: 2/15/20
                System.out.println("Under construction...");
                break;
            case 2:
                borrowBookMenu();
                break;
            case 3:
                returnBookMenu();
                break;
            case 4:
                System.out.println("Bye!!!");
                System.exit(0);
        }

    }

    private static void returnBookMenu() {
        List<Book> borrowedBooks = new ArrayList<>();
        List<Borrow> borrowList = borrowDao.getAll().stream()
                .filter(borrow -> borrow.getUser_id() == currentUser.getId())
                .filter(borrow -> borrow.getReturnDate() == null)
                .collect(Collectors.toList());
        borrowList.stream()
                .map(Borrow::getBook_id)
                .forEach(book_id -> borrowedBooks.add(bookDao.getById(book_id)));


        if (borrowedBooks.size() > 0) {
            while (true) {
                System.out.println();
                System.out.println("------Borrowed Books------");

                AtomicInteger counter = new AtomicInteger(1);
                borrowedBooks.stream()
                        .map(Book::toString) // just one field to show
                        .forEach(x -> System.out.println(counter.getAndIncrement() + " " + x)); // add a row number


                int choice = (int) Console.readNumber("Enter row number: [0 -> previous menu] ", 0, counter.decrementAndGet());
                if (choice == 0)
                    afterLoginMenu();
                selectedBook = borrowedBooks.get(choice - 1);

                Borrow currentBorrow = borrowList.stream()
                        .filter(borrow -> borrow.getBook_id() == selectedBook.getId())
                        .findFirst().get();

                int duration = LocalDate.now().compareTo(currentBorrow.getMustReturnDate().toLocalDate());
                if (duration > 0) {
                    int penalty = (selectedBook.getPenalty() * duration);
                    currentUser.setBudget(currentUser.getBudget() - penalty);
                    userDao.update(currentUser);
                }

                selectedBook.setAvailable(true);

                if (bookDao.update(selectedBook)) {
                    borrowedBooks.remove(selectedBook);
                    currentBorrow.setReturnDate(LocalDateTime.now());
                    borrowDao.update(currentBorrow);
                    selectedBook = null;
                }
            }
        } else {
            System.out.println("There isn't any book to return!!!");
            afterLoginMenu();
        }
    }

    public static void registerMenu() {
        currentUser = new User();

        Console.clearConsole();

        System.out.println("------Register------");
        currentUser.setUsername(Console.readString("Enter your username: "));
        try {
            currentUser.setPassword(Console.setPassword());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(0);
        }
        if (userDao.getByUsername(currentUser.getUsername()).getId() == null) {

            currentUser.setFirstName(Console.readString("Enter your first name: "));
            currentUser.setLastName(Console.readString("Enter your last name: "));
            currentUser.setNationalCode(Console.readString("Enter your national code: "));
            currentUser.setBudget((int) Console.readNumber("Enter your budget: ", 1_000, 1_000_000));
            currentUser.setActive(true);
            currentUser.setDateRegistered(LocalDateTime.now());

            if (Boolean.TRUE.equals(userDao.insert(currentUser))) {
                System.out.println("####################");
                System.out.println("# user registered! #");
                System.out.println("####################");
                Login login = new Login(currentUser.getUsername(), currentUser.getPassword());
                currentUser = userDao.getUserByUserNameAndPassword(login);
                System.out.println("Welcome : " + currentUser.getFirstName() + " " + currentUser.getLastName());

                afterLoginMenu();
            }
        } else {
            System.out.println("########################");
            System.out.println("# username is exist!!! #");
            System.out.println("########################");
            registerMenu();
        }


    }

    private static void borrowBookMenu() {
        List<Book> availableBooks = bookDao.getAll().stream()
                .filter(book -> Boolean.TRUE.equals(book.getAvailable())).collect(Collectors.toList()); // show available books

        if (availableBooks.size() > 0) {
            while (true) {
                System.out.println();
                System.out.println("------Available Books------");

                AtomicInteger counter = new AtomicInteger(1);
                availableBooks.stream()
                        .map(Book::toString) // just one field to show
                        .forEach(x -> System.out.println(counter.getAndIncrement() + " " + x)); // add a row number


                int choice = (int) Console.readNumber("Enter row number: [0 -> previous menu] ", 0, counter.decrementAndGet());
                if (choice == 0)
                    afterLoginMenu();
                selectedBook = availableBooks.get(choice - 1);
                Borrow borrow = new Borrow();
                borrow.setIssueDate(LocalDateTime.now());
                borrow.setMustReturnDate(LocalDateTime.now().plusDays(7));
                borrow.setUser_id(currentUser.getId());
                borrow.setBook_id(selectedBook.getId());

                if (borrowDao.insert(borrow)) {
                    selectedBook.setAvailable(false);
                    availableBooks.remove(selectedBook);
                    bookDao.update(selectedBook);
                    System.out.println("Book " + selectedBook.getTitle() + " By" + selectedBook.getAuthor() + " borrowed!");
                    selectedBook = null;
                }
            }
        } else {
            System.out.println("There isn't any book to borrow!!!");
            afterLoginMenu();
        }
    }

}
