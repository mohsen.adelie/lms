package com.adeli.model;

import java.time.LocalDateTime;

public class Borrow {
    private Integer id;
    private Integer book_id;
    private Integer user_id;
    private LocalDateTime issueDate;
    private LocalDateTime mustReturnDate;
    private LocalDateTime returnDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBook_id() {
        return book_id;
    }

    public void setBook_id(Integer book_id) {
        this.book_id = book_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public LocalDateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDateTime getMustReturnDate() {
        return mustReturnDate;
    }

    public void setMustReturnDate(LocalDateTime mustReturnDate) {
        this.mustReturnDate = mustReturnDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public String toString() {
        return "Borrow{" +
                ", book_id=" + book_id +
                ", user_id=" + user_id +
                ", issueDate=" + issueDate +
                ", returnDate=" + returnDate +
                '}';
    }
}
