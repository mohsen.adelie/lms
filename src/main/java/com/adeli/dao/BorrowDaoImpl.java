package com.adeli.dao;

import com.adeli.model.Borrow;
import com.adeli.util.ConnectionManager;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BorrowDaoImpl implements BorrowDao {
    @Override
    public List<Borrow> getAll() {
        List<Borrow> borrows = new ArrayList<>();

        try (Connection con = ConnectionManager.getInstance().getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM borrows")) {

            while (rs.next()) {
                Borrow borrow = extractBorrowFromResultSet(rs);
                borrows.add(borrow);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrows;
    }

    @Override
    public Borrow getById(Integer id) {
        Borrow borrow = new Borrow();
        try (Connection con = ConnectionManager.getInstance().getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM borrows WHERE borrow_id=?")) {

            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                borrow = extractBorrowFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrow;
    }

    @Override
    public Boolean insert(Borrow borrow) {
        try (Connection con = ConnectionManager.getInstance().getConnection();
             PreparedStatement ps = con.prepareStatement("INSERT INTO borrows VALUES (NULL,?,?,?,?,NULL)")) {

            ps.setInt(1, borrow.getUser_id());
            ps.setInt(2, borrow.getBook_id());
            ps.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now()));
            ps.setTimestamp(4, Timestamp.valueOf(borrow.getMustReturnDate()));

            int i = ps.executeUpdate();

            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Boolean update(Borrow borrow) {
        try (Connection con = ConnectionManager.getInstance().getConnection();
             PreparedStatement ps = con.prepareStatement("UPDATE borrows SET user_id=?, book_id=?, issue_date=?, must_return_date=?, return_date=? WHERE borrow_id=?")) {

            ps.setInt(1, borrow.getUser_id());
            ps.setInt(2, borrow.getBook_id());
            ps.setTimestamp(3, Timestamp.valueOf(borrow.getIssueDate()));
            ps.setTimestamp(4, Timestamp.valueOf(borrow.getMustReturnDate()));
            ps.setTimestamp(5, Timestamp.valueOf(borrow.getReturnDate()));
            ps.setInt(6, borrow.getId());

            int i = ps.executeUpdate();

            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Boolean delete(Borrow borrow) {
        try (Connection con = ConnectionManager.getInstance().getConnection();
             PreparedStatement ps = con.prepareStatement("DELETE FROM borrows WHERE borrow_id=?")) {

            ps.setInt(1, borrow.getId());

            int i = ps.executeUpdate();

            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Borrow extractBorrowFromResultSet(ResultSet rs) throws SQLException {
        Borrow borrow = new Borrow();

        borrow.setId(rs.getInt("borrow_id"));
        borrow.setUser_id(rs.getInt("user_id"));
        borrow.setBook_id(rs.getInt("book_id"));
        borrow.setIssueDate(rs.getTimestamp("issue_date").toLocalDateTime());
        borrow.setMustReturnDate(rs.getTimestamp("must_return_date").toLocalDateTime());
        if (rs.getObject("return_date") != null)
            borrow.setReturnDate(((Timestamp) rs.getObject("return_date")).toLocalDateTime());

        return borrow;
    }

}
