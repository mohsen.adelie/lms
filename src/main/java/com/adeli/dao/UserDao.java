package com.adeli.dao;

import com.adeli.model.Login;
import com.adeli.model.User;

public interface UserDao extends Dao<User, Integer> {
    User getUserByUserNameAndPassword(Login login);

    User getByUsername(String username);
}
