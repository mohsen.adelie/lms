package com.adeli.dao;

import com.adeli.model.Borrow;

public interface BorrowDao extends Dao<Borrow, Integer> {
}
