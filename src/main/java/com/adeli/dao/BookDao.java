package com.adeli.dao;

import com.adeli.model.Book;

public interface BookDao extends Dao<Book, Integer> {
}
