package com.adeli.dao;

import com.adeli.model.Book;
import com.adeli.util.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements BookDao {

    @Override
    public List<Book> getAll() {
        List<Book> books = new ArrayList<>();

        try (Connection con = ConnectionManager.getInstance().getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM books")) {

            while (rs.next()) {
                Book book = extractBookFromResultSet(rs);
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public Book getById(Integer id) {
        Book book = new Book();
        try (Connection con = ConnectionManager.getInstance().getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM books WHERE book_id=?")) {

            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                book = extractBookFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return book;
    }

    @Override
    public Boolean insert(Book book) {
        try (Connection con = ConnectionManager.getInstance().getConnection();
             PreparedStatement ps = con.prepareStatement("INSERT INTO books VALUES (NULL,?,?,?,?,?,?)")) {

            ps.setString(1, book.getTitle());
            ps.setString(2, book.getAuthor());
            ps.setString(3, book.getGenre());
            ps.setBoolean(4, book.getAvailable());
            ps.setInt(5, book.getPrice());
            ps.setInt(6, book.getPenalty());

            int i = ps.executeUpdate();

            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Boolean update(Book book) {
        try (Connection con = ConnectionManager.getInstance().getConnection();
             PreparedStatement ps = con.prepareStatement("UPDATE books SET title=?, author=?, genre=?, available=?, price=?, penalty=? WHERE book_id=?")) {

            ps.setString(1, book.getTitle());
            ps.setString(2, book.getAuthor());
            ps.setString(3, book.getGenre());
            ps.setBoolean(4, book.getAvailable());
            ps.setInt(5, book.getPrice());
            ps.setInt(6, book.getPenalty());
            ps.setInt(7, book.getId());

            int i = ps.executeUpdate();

            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Boolean delete(Book book) {
        try (Connection con = ConnectionManager.getInstance().getConnection();
             PreparedStatement ps = con.prepareStatement("DELETE FROM books WHERE book_id=?")) {

            ps.setInt(1, book.getId());

            int i = ps.executeUpdate();

            if (i == 1) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Book extractBookFromResultSet(ResultSet rs) throws SQLException {
        Book book = new Book();

        book.setId(rs.getInt("book_id"));
        book.setTitle(rs.getString("title"));
        book.setAuthor(rs.getString("author"));
        book.setGenre(rs.getString("genre"));
        book.setAvailable(rs.getBoolean("available"));
        book.setPrice(rs.getInt("price"));
        book.setPenalty(rs.getInt("penalty"));

        return book;
    }

}
