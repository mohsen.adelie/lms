package com.adeli.dao;

import java.util.List;

public interface Dao<T, ID> {
    List<T> getAll();

    T getById(ID id);

    Boolean insert(T t);

    Boolean update(T t);

    Boolean delete(T t);
}
