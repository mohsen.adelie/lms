package com.adeli.view;

import com.adeli.util.PassHash;

import java.util.Scanner;

public class Console {

    private static Scanner scanner = new Scanner(System.in);

    public static String readString(String prompt) {
        String value;
        while (true) {
            System.out.print(prompt);
            value = scanner.next();
            if (!value.equals(""))
                break;
            System.out.print(prompt);
        }
        return value;
    }

    public static double readNumber(String prompt, double min, double max) {
        double value = 0;

        while (true) {
            System.out.print(prompt);
            value = scanner.nextDouble();
            if (value >= min && value <= max)
                break;
            System.out.println("Enter a value between " + (int) min + " and " + (int) max);
        }
        return value;
    }

    public static String readPassword() throws Exception {
        String password = null;

        while (true) {
            System.out.print("Enter password: ");
            password = scanner.next();
            if (!password.equals(""))
                break;
        }
        return PassHash.getMd5(password);
    }

    public static String setPassword() throws Exception {
        String password = null;
        String confirm;
        int loginTry = 1;

        while (loginTry <= 3) {
            System.out.print("(" + loginTry + ") try, Enter password: ");
            password = scanner.next();
            System.out.print("Confirm password: ");
            confirm = scanner.next();
            if (password.equals(confirm))
                break;
            loginTry++;
        }
        if (loginTry != 4)
            return PassHash.getMd5(password);
        else
            throw new Exception("3 wrong attempt for password!");
    }

    public static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
}
